# Gerência de Redes - Tutorial Centreon  
![Centreon.jpg](https://bitbucket.org/repo/Bgk7bMq/images/790553928-Centreon.jpg) 

## Neste tutorial você aprenderá a baixar, instalar e manusear a ferramenta de administração Centreon (versão aberta da comunidade). 


### Sobre o Centreon
Principais Pontos  
```  
- Open Source  
- Monitoramento a nível de camada de aplicação  
- Protocolo SNMP    
- Centralização do Monitoramento através da Web Interface  
```  
O Centreon é uma ferramenta de gerencia de redes muito versátil e de código aberto, é dividido em duas partes principais: O Centreon propriamente dito (integra uma Web Interface a si, utilizada pelo administrador) e a Engine que pode ser a nativa do Centreon (Centreon Engine) ou a Nagios Engine (suporta todos os plugins do Nagios).  

É importante o planejamento da arquitetura antes do desenvolvimento em si e o Centreon possui uma arquitetura bem definida, ele trabalha com plugins que enviam informações dos dispositivos monitorados e o Centreon adquiri essas informações de um banco de dados exibindo as informações em um DashBoard. É importante lembrar que estes puglins podem ser vários todos se reportando a um único banco, dessa forma centralizando as informações da rede, por isso o nome Centreon.  

### Pré-Requisitos (Softwares utilizados pela ferramenta)  
```  
Apache   -  2.2 & 2.4  
GnuTLS   -  >= 2.0  
Net-SNMP -  5.5  
openssl  -  >= 1.0.1e  
PHP      -  >= 5.3.0 & < 5.5  
Qt       -  >= 4.7.4  
RRDtools -  1.4.7  
zlib     -  1.2.3  
```  
### Instalação
Abaixo você verá o passo a passo da instalação que vai desde adquirir o software até configura-lo.  
####Passo 1  - Baixe a versão mais nova do [Centreon](https://download.centreon.com/)
![1.png](https://bitbucket.org/repo/Bgk7bMq/images/919433945-1.png)  
  
####Passo 2 - Instale o sistema CentOS (que vem junto com a ISO do Centreon) em sua maquina
1.  
![4.png](https://bitbucket.org/repo/Bgk7bMq/images/3571760974-4.png)  
2.  
![7.png](https://bitbucket.org/repo/Bgk7bMq/images/3663488318-7.png)  
3.  
![8.png](https://bitbucket.org/repo/Bgk7bMq/images/610944987-8.png)  
4.  
![9.png](https://bitbucket.org/repo/Bgk7bMq/images/2356488298-9.png)  
5.  
![10.png](https://bitbucket.org/repo/Bgk7bMq/images/1503303686-10.png)  
6.  
![11.png](https://bitbucket.org/repo/Bgk7bMq/images/3453955458-11.png)  
7.  
![12.png](https://bitbucket.org/repo/Bgk7bMq/images/1829777695-12.png)  
8.  
![14.png](https://bitbucket.org/repo/Bgk7bMq/images/1736157022-14.png)  
9.  
![15.png](https://bitbucket.org/repo/Bgk7bMq/images/3714530995-15.png)  
10.  
![16.png](https://bitbucket.org/repo/Bgk7bMq/images/2858401817-16.png)  
11.  
![17.png](https://bitbucket.org/repo/Bgk7bMq/images/3510967761-17.png)  
####Passo 3 - Através de um navegador, que possa acessar o IP da maquina com o centreon, faça a configuração inicial da Web Interface da ferramenta (acesse: {IP_DA_MAQUINA}/centreon)
1.  
![18.png](https://bitbucket.org/repo/Bgk7bMq/images/2706277361-18.png)  
2.  
![19.png](https://bitbucket.org/repo/Bgk7bMq/images/1561317766-19.png)  
3.  
![20.png](https://bitbucket.org/repo/Bgk7bMq/images/1494563404-20.png)  
4.  
![21.png](https://bitbucket.org/repo/Bgk7bMq/images/3597756705-21.png)  
5.  
![22.png](https://bitbucket.org/repo/Bgk7bMq/images/3201188615-22.png)  
6.  
![23.png](https://bitbucket.org/repo/Bgk7bMq/images/3216921339-23.png)  
7.  
![24.png](https://bitbucket.org/repo/Bgk7bMq/images/2687678219-24.png)  
8.  
![25.png](https://bitbucket.org/repo/Bgk7bMq/images/3417451674-25.png)  
9.  
![26.png](https://bitbucket.org/repo/Bgk7bMq/images/3543952452-26.png)  
10.  
![27.png](https://bitbucket.org/repo/Bgk7bMq/images/1975166057-27.png)  
11.  
![28.png](https://bitbucket.org/repo/Bgk7bMq/images/2343499068-28.png)  
12.  
![29.png](https://bitbucket.org/repo/Bgk7bMq/images/2198363246-29.png)  
### Passo 4 - Agora você tem o Centreon com a interface gráfica via Navegador disponível, é hora de definir oque queremos monitorar, programar SNMP Traps, intervalos de envios de alertas assim como para onde enviar estes alertas.  
1. Dashboard com as informações de monitoriamento
![30.png](http://i.imgur.com/2kLuwDE.png)
2. Adicionando Hosts para serem monitorados
![31.png](http://i.imgur.com/QSsYLeW.png)
3. Adicionando Serviços para serem monitorados
![31.png](http://i.imgur.com/39uZxSr.png)



### Grupo  
```  
Kevin Barros Costa  
Emídio de Paiva Neto  
```  
### Duvidas?
```
kevin.barros@academico.ifrn.edu.br
emidio.neto@academico.ifrn.edu.br
```